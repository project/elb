# External Link Blocklist

If you want to have extra control about which external links should not
be allowed in your site, this module helps you. With this module you can
create a blocklist of URL's that are forbidden. When a user tries to input
an external link to a non allowed URL, an error will be displayed.

A common use case is when the site is under development and some content editors 
use absolute external links when they should use an internal link. As a consequence,
you might end up having links that point out to wrong domains (as a development server).
This module helps you to mitigate this problem.

## Installation

Install this module as any other. Refer to the [official guide](https://www.drupal.org/docs/extending-drupal/installing-modules) if you need it.

## Configuration

Once enabled, this module provides a field widget for link fields. Just create a field
of type link and select the widget 'External link blocklist' in the 'Manage form display' panel.

Edit the blocklist in the page /admin/config/content/elb. You can also add exceptions for those domains, like
subdomains.

This module has an optional use case for [linkit](https://drupal.org/project/linkit): it alters
the Link's CKEditor button in order to do a validation.

If the users input a URL that is blocklisted they will be informed and they'll have to input something else.

## Author
Alberto G. Viu (alberto@exove.fi) for Exove Ltd.

## Maintainers
Kalle Kipinä (kalle.kipina@exove.fi) for Exove Ltd.
