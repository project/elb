<?php

namespace Drupal\elb\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ExtLinkBlocklistSettingsForm.
 */
class ExtLinkBlocklistSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'elb.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ext_link_blocklist_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('elb.settings');

    $form['blocklist'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Blocklist'),
      '#description' => $this->t('A list of patterns, separated by comma (ie &quot;domain.com, domain.net&quot;).'),
      '#default_value' => $config->get('blocklist'),
    ];

    $form['blocklist_exceptions'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Exceptions'),
      '#description' => $this->t('A list of patterns, separated by comma (ie &quot;domain.com, domain.net&quot;). Can be used to allow linking to a subdomain of a blocklisted domain, for example.'),
      '#default_value' => $config->get('blocklist_exceptions'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('elb.settings')
      ->set('blocklist', $form_state->getValue('blocklist'))
      ->set('blocklist_exceptions', $form_state->getValue('blocklist_exceptions'))
      ->save();
  }

}
