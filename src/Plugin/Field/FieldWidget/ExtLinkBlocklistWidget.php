<?php

namespace Drupal\elb\Plugin\Field\FieldWidget;

use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'ext_link_blocklist_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "ext_link_blocklist_widget",
 *   module = "elb",
 *   label = @Translation("External link blocklist"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class ExtLinkBlocklistWidget extends LinkWidget {

  /**
   * Checks the uri against the blocklist.
   */
  public static function validateUriElement($element, FormStateInterface $form_state, $form) {
    $uri = static::getUserEnteredStringAsUri($element['#value']);
    parent::validateUriElement($element, $form_state, $form);

    if ($form_state->getErrors()) {
      // If there are already errors, continue.
      return;
    }

    if (\Drupal::service('elb.blocklist')->isBlocklisted($uri)) {
      $form_state->setError($element, t('This URL contains a pattern that is blocklisted.'));
      return;
    }
  }

}
