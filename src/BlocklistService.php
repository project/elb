<?php

namespace Drupal\elb;

use Drupal\Core\Config\ConfigFactory;

/**
 * Class BlocklistService.
 */
class BlocklistService {

  /**
   * The blocklist.
   *
   * @var []string
   */
  protected $blocklist;

  /**
   * Blocklist exceptions.
   *
   * @var []string
   */
  protected $blocklist_exceptions;

  /**
   * The Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config_factory;

  /**
   * Constructs a new BlocklistService object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactory $config_factory) {
    $this->config_factory = $config_factory;
    $this->blocklist = $this->getBlocklist();
    $this->blocklist_exceptions = $this->getExceptions();
  }

  /**
   * Return the blocklist.
   *
   * @return [] string
   */
  public function getBlocklist() {
    $blocklist = $this->config_factory->get('elb.settings')->get('blocklist');
    $backlist_array = explode(',', $blocklist);
    return array_filter(array_map('trim', $backlist_array));
  }

  /**
   * Return the blocklist exceptions.
   *
   * @return [] string
   */
  public function getExceptions() {
    $exceptions = $this->config_factory->get('elb.settings')->get('blocklist_exceptions');
    $exceptions_array = explode(',', $exceptions);
    return array_filter(array_map('trim', $exceptions_array));
  }

  /**
   * Checks if the given $uri is blocklisted.
   *
   * @param string $uri
   *   The uri string.
   */
  public function isBlocklisted($uri) {
    foreach ($this->blocklist as $prohibited_string) {
      if (substr_count($uri, $prohibited_string) && !$this->isExceptedFromBlocklist($uri)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Checks if the given $uri is excepted from the blocklist.
   *
   * @param string $uri
   *   The uri string.
   */
  public function isExceptedFromBlocklist($uri) {
    foreach ($this->blocklist_exceptions as $exception_string) {
      if (substr_count($uri, $exception_string)) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
